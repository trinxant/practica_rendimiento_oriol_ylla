# Práctica Rendimiento MPWAR

Para ejecutar el proyecto hay que tener una máquina virtual provisionada con lo siguiente:

- RabbitMQ
- ElasticSearch
- PHP 7.1
- Composer
- Apache
- MySQL
- Redis

En mi caso he usado la maquina virtual proporcionada por el profesor, en la cual faltaba por instalar ElasticSearch, por lo que habria
que instalar-lo en caso de usar esta máquina.

El primer paso es clonar el proyecto en un directorio visible por apache.

También hay que crear la base de datos que va a usar la aplicación.

``` sql
CREATE DATABASE performance_db;

CREATE TABLE Images (
    id varchar(13) NOT NULL PRIMARY KEY,
    extension varchar(3) NOT NULL,
    transformation varchar(255)
);
```

instalamos las dependencias con composer

``` php
$ composer install
```

Arrancamos los consumers que van a consumir de la colas de RabbitMQ.
Para mi práctica he creado 4 colas para redimensionar imagenes, cada una a un tamaño distinto, y tres colas para
aplicar filtros: escala de grises, sepia y inversion de colores.

des de la raíz del proyecto:
```
php workers/filtersWorker.php grayscale // el parametro indica el filtro, puede ser: grayscale, negate o sepia

php workers/resizeWorker.php 2 // el parametro indica numero de escalaje de la imgen, puede ser: 2, 3, 4 o 5
```
hay que usar un terminal distinto para cada uno, ya que el processo queda en suspension.

por defecto basta con activar un solo consumer por cada cola, pero para mejorar el rendimiento podemos iniciar varios en un caso concreto.

Y listo! Ya podemos entrar en la aplicacion

```
http://{host}/practica_rendimiento_Oriol_Ylla
```

## Funcionamiento

La aplicación permite subir imágenes arrastrandolas al espacio indicado. Una vez subidas podemos refrescar
la página y apareceran esas y sus transformaciones, con una etiqueta indicando cual es cual, ya que sino no se podria
distinguir entre las distintas transformaciones. Tambien hay un botón apra abrir la imagen en una pestaña nueva.

Cuando las imagenes se suben, se crea un tag automáticamente, el cual indica la transformación. De modo que podremos
filtrar las imagenes a través de ElasticSearch por sus transformaciones. (resize2m resize3, resize4, resize5, grayscale, sepia, negate).

Si modificamos los campos que hay encima de la imagen y le damos a editar, estos valores sustituirán los antiguos en ElasticSearch. Por lo que
podremos setear tags propios y añadir una descripción a la imagen.
Introduciendo texto en el input de Search. se filtrarán las imagenes quando haya una coincidencia con los Tags o la Descripcion.

## Profiling con Blackfire

para preparar el profiling con black fire hay que hacer lo siguiente:
```
    // instalar el agent de Black fire. Esto te pedira las credenciales del server.
    sudo blackfire-agent -register
    
    // tras esta configuración, hay que reiniciar el servicio
    sudo /etc/init.d/blackfire-agent restart
    
    // con este comando inicializamos el cliente
    //nos pedirá las credenciales del cliente
    blackfire config
```

El resultado es el siguiente:

[Profiling del punto de entrada](https://i.imgur.com/nypMNTp.png) http://{host}/practica_rendimiento_Oriol_Ylla

[Profiling de la busqueda en ElasticSearch](https://i.imgur.com/wGqhOTk.png) http://{host}/practica_rendimiento_Oriol_Ylla?search-input=grayscale

No se ha podido hacer el profiling de los endpoints que van con peticion POST (edit.php y upload.php), ya que si estos no tienen los parametros
definidos terminan el proceso sin hacer nada.

En los gráficos de black fire, no se observa ningún cuello de botella o llamadas a funciones innecesarias. Por lo que no parece
que se pueda mejorar mucho en cuanto a rendimiento a parte de probar a jugar con el numero de consumers para mejorar el rendimiento en casos
en que se suban muchas imágenes de golpe.

Donde si que hay margen de mejora es en la arquitectura. Al principio intenté seguir principios SOLID para el desarrollo, pero a medida que me quedaba
sin tiempo he ido centrándome mas en cumplir funcionalidades que en la buena arquitectura. Con lo cual es un tema que queda pendiente. Luego valdria la pena
volver a hacer un profiling.
