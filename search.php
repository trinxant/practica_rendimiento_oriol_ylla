<?php

require 'vendor/autoload.php';

use Elasticsearch\ClientBuilder;

$client = ClientBuilder::create()->build();


$params = [
    'index' => 'images',
    'type' => 'image',
    'body' => [
        'query' => [
            'multi_match' => [
                'query' => $_GET['search-input'],
                'fields' => ['desc', 'tags']
            ]
        ]
    ]
];

try {
    $response = $client->search($params);
} catch(Exception $e) {
    echo 'Caught exception: ',  $e->getMessage(), "\n";
}

$hits = $response['hits']['hits'];
$ids = [];
foreach($hits as $hit) {
    $ids[] = $hit['_id'];
}

header('Content-Type: application/json');
echo json_encode($ids);