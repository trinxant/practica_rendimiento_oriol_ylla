<?php
require_once __DIR__ . '/vendor/autoload.php';

use App\Infrastructure\ImageRepository;

$imageRepository = new ImageRepository();
$images = $imageRepository->getAll();

?>

<!DOCTYPE html>
<html>

<head>
    <script src="js/jquery.js"></script>
    <script src="js/dropzone.js"></script>
    <script src="js/main.js"></script>
    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/dropzone.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
</head>

<body>

<form action="upload.php" class="dropzone">
    <inptut>
        <div class="fallback">
            <input name="file" type="file" multiple />
        </div>
    </inptut>
</form>

<input class="search-input" type="text" placeholder="Search." name="search-input">

<div class="gallery">
    <?php
        foreach($images as $image) {
            $path = $image['path'];
            $transf = $image['transformation'];
            $id = $image['id'];
            ?>
            <div class="gallery-item" id="<?php echo $id ?>" style="background-image: url(<?php echo "images/$path" ?>)">
                <div class="item-info">
                    <div class="label <?php echo $transf?>"> <?php echo $transf ?> </div>
                    <form class="edit-input">
                        <input type="text" name="description" placeholder="Description."><br/>
                        <input type="text" name="tags" placeholder="Tags"><br/>
                        <input type="submit" value="Edit">
                    </form>
                    <button class="openBtn" name="<?php echo "images/$path"?>" type="button">Open</button>
                </div>
            </div>
            <?php
        }
    ?>
</div>

</body>

</html>