CREATE DATABASE performance_db;

CREATE TABLE Images (
    id varchar(13) NOT NULL PRIMARY KEY,
    extension varchar(3) NOT NULL,
    transformation varchar(255)
);