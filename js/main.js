$( document ).ready(function() {
    $('.search-input').on('input', function () {
        var input = $(this).val();
        console.log(input);
        $.ajax({
            url: 'search.php',
            method: 'GET',
            data: { 'search-input': input },
            success: function (res) {
                filterImgsByIds(res);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });
    $('.edit-input').submit(function(e){
        $.ajax({
            url: 'edit.php',
            method: 'POST',
            data: {
                id: $(this).closest('.gallery-item')[0].id,
                desc: $(this).closest('form').find("input[name=description]").val(),
                tags: $(this).closest('form').find("input[name=tags]").val()
            },
            success: function (res) {
                console.log(res);
            },
            error: function (err) {
                console.log(err);
            }
        });
        $(this).closest('form').find("input[type=text]").val("");
        return false;
    });
    $('.openBtn').click(function(e) {
        var path = e.currentTarget.attributes[1].nodeValue;
        openImg(path);
    });
});

function filterImgsByIds(ids)
{
    var items = $('.gallery-item');
    if(ids.length === 0) {
        items.show();
        return;
    }
    items.hide();
    ids.map(function (id) {
        $('#' + id).show();
    });
}
function openImg ($path) {
    console.log($path);
    window.open($path, '_blank');
}

