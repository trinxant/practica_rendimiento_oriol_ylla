<?php

use App\Application\RabbitMQInitialize;
use App\Domain\ValueObjects\QueueID;
use App\Infrastructure\AMQPReceiver;
use App\Infrastructure\ImageRepository;
use Gregwar\Image\Image;
use App\Infrastructure\ESImageIndexer;

require_once  __DIR__ . '/../vendor/autoload.php';

validateInput($argv[1]);

$imageRepository = new ImageRepository();

$imageIndexer = new ESImageIndexer();

$channel = RabbitMQInitialize::run();
$receiver = new AMQPReceiver($channel);
$queueId = new QueueID($argv[1]);
$receiver->add($queueId, getCallback($argv[1], $imageRepository, $imageIndexer));
while(count($channel->callbacks)) {
    $channel->wait();
}

function getCallback($filter, ImageRepository $imageRepository, ESImageIndexer $imageIndexer) {
    return function ($msg) use ($filter, $imageRepository, $imageIndexer) {

        $payload = explode(" ", $msg->body);
        $path = $payload[0];
        $originalName = $payload[1];
        $ext = pathinfo($originalName, PATHINFO_EXTENSION);
        $id = uniqid();
        $imageIndexer->run($id, '', $filter, false);
        $transformName = $id . '.' . $ext;
        $imageRepository->save($id, $ext, $filter);
        switch ($filter) {
            case 'grayscale' :
                Image::open($path . $originalName)
                    ->grayscale()
                    ->save($path . $transformName);
                break;
            case 'negate' :
                Image::open($path . $originalName)
                    ->negate()
                    ->save($path . $transformName);
                break;
            case 'sepia' :
                Image::open($path . $originalName)
                    ->sepia()
                    ->save($path . $transformName);
                break;
        }
    };
}

function validateInput($val)
{
    if(!isset($val)) {
        echo "The input argument must be a valid filter: negate, grayscale, sepia.";
        die();
    }
    if ($val != 'negate' && $val != 'sepia' && $val != 'grayscale') {
        echo "The input argument must be a valid filter: negate, grayscale, sepia.";
        die();
    }
}
