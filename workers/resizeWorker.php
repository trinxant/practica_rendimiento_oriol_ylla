<?php

use App\Application\RabbitMQInitialize;
use App\Domain\ValueObjects\QueueID;
use App\Infrastructure\AMQPReceiver;
use App\Infrastructure\ImageRepository;
use App\Infrastructure\ESImageIndexer;
use Gregwar\Image\Image;

require_once  __DIR__ . '/../vendor/autoload.php';

validateInput($argv[1]);

$imageRepository = new ImageRepository();
$imageIndexer = new ESImageIndexer();

$channel = RabbitMQInitialize::run();
$receiver = new AMQPReceiver($channel);
$queueId = new QueueID("resize-x$argv[1]");

$receiver->add($queueId, getCallback($argv[1], $imageRepository, $imageIndexer));

while(count($channel->callbacks)) {
    $channel->wait();
}

function getCallback($num, ImageRepository $imageRepository, ESImageIndexer $imageIndexer) {
    return function ($msg) use ($num, $imageRepository, $imageIndexer) {
        $payload = explode(" ", $msg->body);
        $path = $payload[0];
        $originalName = $payload[1];
        $ext = pathinfo($originalName, PATHINFO_EXTENSION);
        $id = uniqid();
        $transformName = $id . '.' . $ext;
        $rFactor = 200 * intval($num);
        $imageIndexer->run($id, '', "resize$num", false);
        $imageRepository->save($id, $ext, "resize$num");
        Image::open($path . $originalName)
            ->cropResize($rFactor, $rFactor)
            ->save($path . $transformName);
    };
}

function validateInput($val)
{
    if(!isset($val)) {
        echo "You must input a resize factor: 2, 3, 4 or 5.";
        die();
    }
    if ($val != '2' && $val != '3' && $val != '4' && $val != '5') {
        echo "The input argument must be 2, 3, 4 or 5.";
        die();
    }
}
