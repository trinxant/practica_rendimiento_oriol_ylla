<?php

require 'vendor/autoload.php';

use App\Infrastructure\ESImageIndexer;

$imageIndexer = new ESImageIndexer();
$response = $imageIndexer->run($_POST['id'], $_POST['desc'], $_POST['tags'], true);


header('Content-Type: application/json');
echo json_encode($response);
