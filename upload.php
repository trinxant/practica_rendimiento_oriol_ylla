<?php

require_once __DIR__ . '/vendor/autoload.php';

use App\Application\ImageSaver;

define("ROOT_PATH", __DIR__);

if (!empty($_FILES)){
    $saver = new ImageSaver();
    $saver->save($_FILES);
}
