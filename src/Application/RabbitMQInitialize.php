<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 22/5/18
 * Time: 20:33
 */

namespace App\Application;

use App\Infrastructure\AMQPConnection;
use App\Domain\ValueObjects\QueueID;
use App\Infrastructure\AMQPQueueRegister;
use PhpAmqpLib\Channel\AMQPChannel;

final class RabbitMQInitialize
{
    static function run(): AMQPChannel
    {
        $connection = new AMQPConnection();
        $channel = $connection->channel();

        AMQPQueueRegister::register($channel, new QueueID('resize-x2'));
        AMQPQueueRegister::register($channel, new QueueID('resize-x3'));
        AMQPQueueRegister::register($channel, new QueueID('resize-x4'));
        AMQPQueueRegister::register($channel, new QueueID('resize-x5'));
        AMQPQueueRegister::register($channel, new QueueID('sepia'));
        AMQPQueueRegister::register($channel, new QueueID('negate'));
        AMQPQueueRegister::register($channel, new QueueID('grayscale'));

        return $channel;

    }
}