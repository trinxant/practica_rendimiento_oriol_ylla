<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 24/5/18
 * Time: 19:21
 */

namespace App\Application;


use App\Domain\ValueObjects\QueueID;
use App\Infrastructure\AMQPPublisher;
use App\Infrastructure\ESImageIndexer;
use App\Infrastructure\ImageRepository;
use PhpAmqpLib\Message\AMQPMessage;

final class ImageSaver
{
    const DS = DIRECTORY_SEPARATOR;
    const STORE_FOLDER = 'images';
    private $publisher;
    private $imageRepository;
    private $imageIndexer;

    public function __construct()
    {
        $channel = RabbitMQInitialize::run();
        $this->publisher = new AMQPPublisher($channel);
        $this->imageRepository = new ImageRepository();
        $this->imageIndexer = new ESImageIndexer();
    }

    public function save($file)
    {
        if (!empty($file)) {
            var_dump($file);
            $tempFile = $file['file']['tmp_name'];
            $targetPath = ROOT_PATH . self::DS. self::STORE_FOLDER . self::DS;
            $ext = pathinfo($file['file']['name'], PATHINFO_EXTENSION);
            $id = uniqid();
            $filename = $id . '.' . $ext;
            $targetFile = $targetPath . $filename;
            move_uploaded_file($tempFile,$targetFile);

            $this->imageIndexer->run($id, null, ['original'], false);

            $this->imageRepository->save($id, $ext, 'original');

            $payload = [ $targetPath,  $filename ];

            $msg = new AMQPMessage(implode(" ", $payload));

            $this->publisher->__invoke($msg, new QueueID('resize-x2'));
            $this->publisher->__invoke($msg, new QueueID('resize-x3'));
            $this->publisher->__invoke($msg, new QueueID('resize-x4'));
            $this->publisher->__invoke($msg, new QueueID('resize-x5'));
            $this->publisher->__invoke($msg, new QueueID('sepia'));
            $this->publisher->__invoke($msg, new QueueID('grayscale'));
            $this->publisher->__invoke($msg, new QueueID('negate'));

        }
        else {
            echo 'empty';
        }
    }

}