<?php

namespace App\Infrastructure;

use PDO;
use PDOException;

final class ImageRepository
{

    private $connection;

    public function __construct()
    {
        $host = '127.0.0.1';
        $db = 'performance_db';
        $user = 'root';
        $pass = 'root';
        $charset = 'utf8mb4';

        $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
        try {
            $this->connection = new PDO($dsn, $user, $pass);
        } catch(PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }

    public function save($id, $extension, $transformation)
    {
        $sql = "INSERT INTO Images (id, extension, transformation) VALUES (?,?,?)";
        $stmt= $this->connection->prepare($sql);
        $stmt->execute([$id, $extension, $transformation]);
    }

    public function getAll() {
        $sql = 'SELECT * FROM Images';
        $images = [];
        foreach ($this->connection->query($sql) as $row) {
            $images[] = [
                'id' => $row['id'],
                'path' => $row['id'] . '.' . $row['extension'],
                'transformation' => $row['transformation']
            ];
        }
        return $images;
    }

}