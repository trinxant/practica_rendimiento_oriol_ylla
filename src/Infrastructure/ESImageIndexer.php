<?php

namespace App\Infrastructure;


use Elasticsearch\ClientBuilder;
use http\Exception;

final class ESImageIndexer
{
    private $ESClient;

    public function __construct()
    {
        $this->ESClient = ClientBuilder::create()->build();
    }

    public function run($id, $desc, $tags, $update) {
        if($update) {

            $params = [
                'index' => 'images',
                'type' => 'image',
                'id' => $id,
                'body' => [
                    'doc' => [
                    ]
                ]
            ];

            if($tags != '') $params['body']['doc']['tags'] = $tags;
            if($desc != '') $params['body']['doc']['desc'] = $desc;

            $response = $this->ESClient->update($params);
        } else {
            $params = [
                'index' => 'images',
                'type' => 'image',
                'id' => $id,
                'body' => [
                    'tags' => $tags,
                    '$desc' => $desc
                ]
            ];
            $response = '';
            try {
                $response = $this->ESClient->index($params);
            } catch(Exception $e) {
                echo 'Caught exception: ',  $e->getMessage(), "\n";
            }

        }
        return $response;
    }
}