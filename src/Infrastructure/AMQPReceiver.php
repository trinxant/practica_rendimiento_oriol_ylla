<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 22/5/18
 * Time: 19:58
 */

namespace App\Infrastructure;


use App\Domain\ValueObjects\QueueID;
use PhpAmqpLib\Channel\AMQPChannel;

final class AMQPReceiver
{
    private $channel;

    public function __construct(AMQPChannel $channel)
    {
        $this->channel = $channel;
    }

    public function add(QueueID $queueId, $callback)
    {
        $this->channel->basic_consume($queueId->value(), '', false, true, false, false, $callback);
    }

    public function close(): void
    {
        $this->channel->close();
    }

    private function defaultCallback()
    {
        return function ($msg) {
            echo $msg->body;
        };
    }

}