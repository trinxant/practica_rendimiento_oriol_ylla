<?php

namespace App\Infrastructure;

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Channel\AMQPChannel;

final class AMQPConnection
{
    private $connection;

    public function __construct()
    {
        $this->connection = new AMQPStreamConnection('localhost', 5672, 'admin', 'admin');
    }

    public function channel(): AMQPChannel
    {
        return $this->connection->channel();
    }
}