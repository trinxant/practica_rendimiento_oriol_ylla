<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 22/5/18
 * Time: 19:15
 */

namespace App\Infrastructure;

use App\Domain\ValueObjects\QueueID;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Message\AMQPMessage;

final class AMQPPublisher
{
    private $channel;

    public function __construct(AMQPChannel $channel)
    {
        $this->channel = $channel;
    }

    public function __invoke(AMQPMessage $msg, QueueID $queueId)
    {
        $this->channel->basic_publish($msg, '', $queueId->value());
    }

    public function close(): void
    {
        $this->channel->close();
    }
}