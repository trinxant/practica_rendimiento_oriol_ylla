<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 22/5/18
 * Time: 19:42
 */

namespace App\Infrastructure;

use App\Domain\ValueObjects\QueueID;
use PhpAmqpLib\Channel\AMQPChannel;

final class AMQPQueueRegister
{
    static function register(AMQPChannel $channel, QueueID $id): void
    {
        $channel->queue_declare($id->value(), false, false, false, false);
    }
}