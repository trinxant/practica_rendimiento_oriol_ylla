<?php
/**
 * Created by PhpStorm.
 * User: oylla
 * Date: 22/5/18
 * Time: 19:31
 */

namespace App\Domain\ValueObjects;

final class QueueID
{
    private $value;
    public function __construct(string $id)
    {
        $this->value = $id;
    }

    public function value(): string
    {
        return $this->value;
    }
}